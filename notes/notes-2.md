### Sample note 
---

1. **List item 1** 
	- Point 1
	- Example:  
	![Alt text](other_images/sample-imgs/sample-imgs-1.png)
2. **List item 2**
	- Point 2
	- Example
	![Alt text](other_images/sample-imgs/sample-imgs-2.png)


----

### An attempt with `./img/` and `.img/`


1. **List item 1** 
	- Point 1
	- Example:  
	![Alt text](./other_images/sample-imgs/sample-imgs-1.png)
2. **List item 2**
	- Point 2
	- Example
	![Alt text](.other_images/sample-imgs/sample-imgs-2.png)

